import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np

class DeepQNet(nn.Module):
    def __init__(self, lr, input_dim, fc1_dim, fc2_dim, action_n):
        super(DeepQNet, self).__init__()        
        self.fc1 = nn.Linear(*input_dim, fc1_dim)
        self.fc2 = nn.Linear(fc1_dim, fc2_dim)
        self.fc3 = nn.Linear(fc2_dim, action_n)
        self.optimizer = optim.Adam(self.parameters(), lr=lr)
        self.loss = nn.MSELoss()
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else
                                   'cpu')
        self.to(self.device)
        
    def forward(self, state):
        x = F.relu(self.fc1(state))
        x = F.relu(self.fc2(x))
        actions = self.fc3(x)
        
        return actions
    
class Agent():
    def __init__(self, gamma, lr, input_dims, batch_size, action_n,
                 epsilon=1.0, mem_size=100000, epsilon_min=0.01, dec=1e-3):
        self.gamma = gamma
        self.epsilon = epsilon
        self.epsilon_min = epsilon_min
        self.epsilon_dec = dec
        self.lr = lr
        self.action_space = [i for i in range(action_n)]
        self.mem_size = mem_size
        self.batch_size = batch_size
        self.mem_counter = 0
        self.Q_eval = DeepQNet(lr, input_dims, fc1_dim=256, fc2_dim=256,
                               action_n=action_n)
        self.state_memory = np.zeros((mem_size, *input_dims), dtype=np.float32)
        self.next_state_memory = np.zeros((mem_size, *input_dims), 
                                          dtype=np.float32)
        self.action_memory = np.zeros(mem_size, dtype=np.int32)
        self.reward_memory = np.zeros(mem_size, dtype=np.float32)
        self.terminal_memory = np.zeros(mem_size, dtype=np.bool)
        
    def store_transition(self, state, action, reward, next_state, done):
        ind = self.mem_counter % self.mem_size
        self.state_memory[ind] = state
        self.next_state_memory[ind] = next_state
        self.action_memory[ind] = action
        self.reward_memory[ind] = reward
        self.terminal_memory[ind] = done
        
        self.mem_counter += 1
        
    def select_action(self, obs):
        if np.random.random() > self.epsilon:
            state = torch.tensor([obs]).to(self.Q_eval.device)
            actions = self.Q_eval.forward(state)
            action = torch.argmax(actions).item()
        else:
            action = np.random.choice(self.action_space)
        
        return action
    
    def learn(self):
        if self.mem_counter < self.batch_size:
            return
        
        self.Q_eval.optimizer.zero_grad()
        max_mem = min(self.mem_counter, self.mem_size)
        batch = np.random.choice(max_mem, self.batch_size, replace=False)
        batch_ind = np.arange(self.batch_size, dtype=np.int32)
        state_batch = torch.tensor(self.state_memory[batch]).to(
                self.Q_eval.device)
        next_state_batch = torch.tensor(self.next_state_memory[batch]).to(
                self.Q_eval.device)
        reward_batch = torch.tensor(self.reward_memory[batch]).to(
                self.Q_eval.device)
        terminal_batch = torch.tensor(self.terminal_memory[batch]).to(
                self.Q_eval.device)
        action_batch = self.action_memory[batch]
        
        q_eval = self.Q_eval.forward(state_batch)[batch_ind, action_batch]
        q_next = self.Q_eval.forward(next_state_batch)
        q_next[terminal_batch] = 0.0
        
        q_target = reward_batch + self.gamma * torch.max(q_next, dim=1)[0]
        loss = self.Q_eval.loss(q_target, q_eval).to(self.Q_eval.device)
        loss.backward()
        
        self.Q_eval.optimizer.step()
        self.epsilon = self.epsilon - self.epsilon_dec if self.epsilon \
                                > self.epsilon_min else self.epsilon_min
        
    def save_model(self, path):
        torch.save(self.Q_eval, path)
        print('-----MODEL SAVED-----')
    
    def load_model(self, path):
        self.Q_eval = torch.load(path)
        self.Q_eval.eval()
        