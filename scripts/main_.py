import gym
from deepQNet_torch import Agent
from visual import save_plot

if __name__ == '__main__':
    env = gym.make('LunarLander-v2')
    action_n = env.action_space.n
    agent = Agent(gamma=0.95, lr=1e-4, input_dims=[8], action_n=action_n,
                  batch_size=64, dec=1e-4)
    
    scores, eps_history = [], []
    epochs = 20
    load_dqn = True #True for playing with trained DQN, False for training
    load_path = 'DQN_torch_n2000_dynamic.pt'
    save_path = 'DQN_torch_n2000_dynamic.pt'
    
    if load_dqn: 
        agent.load_model(load_path)
        agent.epsilon = agent.epsilon_min
    
    if not load_dqn:
        for i in range(epochs):
            score = 0
            done = False
            obs = env.reset()
            while not done:
                action = agent.select_action(obs)
                obs_next, reward, done, info = env.step(action)
                score += reward
                agent.store_transition(obs, action, reward, obs_next, done)
                agent.learn()
                obs = obs_next
            print('game ', i+1, ' score: ', score, ' epsilon: %.3f' 
                  %agent.epsilon, ' alpha: %.4f' %agent.lr)
            scores.append(score)
            eps_history.append(agent.epsilon)
            if i % 500 == 0 and i > 0:
                agent.gamma += 0.01
        x = [i+1 for i in range(epochs)]
        save_plot(x, scores, eps_history, 'LunarLander Learning dynamicLR g99.png')
        agent.save_model(save_path)
    else:
        for i in range(epochs):
            score = 0
            done = False
            obs = env.reset()
            while not done:
                action = agent.select_action(obs)
                obs_next, reward, done, info = env.step(action)
                score += reward
                obs = obs_next
                #env.render() #Uncomment for visual representation of playing
            scores.append(score)
            eps_history.append(agent.epsilon)
            print('game ', i+1, ' score: ', score)
        x = [i+1 for i in range(epochs)]
        save_plot(x, scores, eps_history, 'LunarLander Trained dynamic.png')